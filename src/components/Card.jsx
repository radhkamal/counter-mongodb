import React from 'react'
import Header from './Header'
import Counter from './Counter'

const Card = () => {
  return (
    <div className='card-wrap'>
      <Header name="Counter using MongoDB" />
      <Counter />
    </div>
  )
}

export default Card
