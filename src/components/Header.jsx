import React from 'react'

const Header = ({ name }) => {
  return (
    <div className="header-wrap">
      <h1 className='header'>{name}</h1>
    </div>
  )
}

export default Header;
